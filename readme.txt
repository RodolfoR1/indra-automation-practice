
Executar sem geração de relatório
 - npm run test

Execução do allure:
 - npx cypress run --env allure=true

Geração do Relatorio
 - allure generate allure-results

Abrir o Relatório
 - allure open allure-report