
import compra from '../pages/CompraPage'

describe('Realizar Compra', function() {
    //Carregar massa de testes para salvar endereco
    before(function() {
        cy.fixture('endereco').then((e)=>{
            this.endereco = e
        })
    })

    it('Realizar Login', function() {
        //realizar o login
        compra.open()
        compra.login()
        compra.preencherFormularioLogin()
        compra.botaoEntrar()
        compra.tituloPaginaUsuario('My account')
    })

    it('Realizar compra sem estar previamente logado', function(){
        //Abri pagina do site
        compra.open()

        //Selecionar Item de Compra e adicionar item ao carrinho
        compra.selecionarOpcao()
        compra.addItemCarrinho()
        compra.clicarBotaoIrParaCarrinho()
        compra.clicarSeguirCompra()

        //Preencher dados do login para prosseguir
        compra.preencherFormularioLogin()
        compra.botaoEntrar()

        //Avançar a etapa de definição do endereço
        compra.submitCheckout()

        //Definir os termos de envio
        compra.marcarTermosDeServico()
        compra.submitCheckout()

        //Definir a forma de pagamento
        compra.pagamentoTransferenciaBancaria()
        compra.submitCheckout()

        // Confirmar o pagamento
        compra.mensagemConfirmarCompra('Order confirmation')
    })

    it('Realizar compra previamente logado', function(){
        //realizar o login e ir para a página inicial
        compra.open()
        compra.login()
        compra.preencherFormularioLogin()
        compra.botaoEntrar()
        compra.home()

        //Selecionando item para compra
        compra.selecionarOpcao()

        //Add item para compra e direcionar para o carrinho 
        compra.addItemCarrinho()
        compra.clicarBotaoIrParaCarrinho()
        compra.clicarSeguirCompra()
        compra.submitCheckout()

        // Definir termos de envio do produto
        compra.marcarTermosDeServico()
        compra.submitCheckout()
        
        // Definir a forma de pagamento
        compra.pagamentoTransferenciaBancaria()
        compra.submitCheckout()

        // Confirmar o pagamento
        compra.mensagemConfirmarCompra('Order confirmation')
    })

    it('Realizar compra e cadastrando endereço de entrega', function(){
        //realizar o login e ir para a página inicial
        compra.open()
        compra.login()
        compra.preencherFormularioLogin()
        compra.botaoEntrar()
        compra.home()

        //Buscar item para compra
        compra.selecionarOpcao()

        //Add item para compra e direcionar para o carrinho 
        compra.addItemCarrinho()
        compra.clicarBotaoIrParaCarrinho()
        compra.clicarSeguirCompra()

        //Add Endereço de entrega
        compra.botaoAddEndereco()
        compra.preencherNovoEndereco(this.endereco.novo_endereco)
        compra.salvarEndereco()
        compra.submitCheckout() //avançar para opçao de envio

        // Definir termos de envio do produto
        compra.marcarTermosDeServico()
        compra.submitCheckout()
        
        // Definir a forma de pagamento
        compra.pagamentoTransferenciaBancaria()
        compra.submitCheckout()

        // Confirmar o pagamento
        compra.mensagemConfirmarCompra('Order confirmation')
    })
})