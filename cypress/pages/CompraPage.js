class CompraPage{
    
    open(){
        cy.visit('http://automationpractice.com/index.php')
    }

    home(){
        cy.get('.breadcrumb .home').click()
    }

    selecionarOpcao(){
        cy.get('div[id="block_top_menu"] ul li a[title="Dresses"]').eq(1).invoke('show').click()
        cy.get('ul a[title="Casual Dresses"]').eq(2).invoke('show').click()
    }

    addItemCarrinho(){
        cy.get('.ajax_block_product').eq(0).invoke('show').click()
        cy.get('.product_list li div[class=product-container] a[title="Add to cart"]').eq(0).invoke('show').click()
    }

    clicarBotaoIrParaCarrinho(){
        cy.get('.button-container a[title="Proceed to checkout"]').click()
    }

    login(){
        cy.get('a[title="Log in to your customer account"]').click()
    }

    preencherFormularioLogin(){
        cy.contains('.page-heading', 'Authentication').should('be.visible')

        cy.get('input[name="email"][id="email"]').type('automacao@email.com')
        cy.get('input[name="passwd"][id="passwd"]').type('Auto123123')
    }

    botaoEntrar(){
        cy.get('form button[type="submit"][id="SubmitLogin"]').click()
    }

    submitCheckout(){
        cy.get('form .cart_navigation button[type="submit"]').click()
    }

    clicarSeguirCompra(){
        cy.get('.standard-checkout').click()
    }

    tituloPaginaUsuario(message){
        cy.contains('div[id="center_column"] [class="page-heading"]', message).should('be.visible')
    }

    marcarTermosDeServico(){
        cy.get('input[type="checkbox"]').check()
    }

    pagamentoTransferenciaBancaria(){
        cy.get('.payment_module .bankwire').click()
    }

    mensagemConfirmarCompra(expectedMessage){
        cy.get('#center_column .page-heading').should('have.text', expectedMessage)
    }

    botaoAddEndereco(){
        cy.get('.address_add a[title=Add]').click()
    }

    preencherNovoEndereco(endereco){
        cy.get('input[name="firstname"]').type(endereco.primeiro_nome)
        cy.get('input[name="lastname"]').type(endereco.sobrenome)
        cy.get('input[name="address1"]').type(endereco.endereco)
        cy.get('input[name="city"]').type(endereco.cidade)

        cy.get('select[id=id_state]').select(endereco.estado)
        // cy.get('input[name="id_state"]').select(endereco.estado)
        cy.get('input[name="postcode"]').type(endereco.codigo_postal)
        cy.get('select[name="id_country"]').select(endereco.pais)
        cy.get('input[name="phone"]').type(endereco.telefone)
        cy.get('input[name="phone_mobile"]').type(endereco.celular)

        cy.get('textarea[name="other"]').type(endereco.complemento)
        cy.get('input[name="alias"]').type(endereco.titulo_endereco)
    }

    salvarEndereco(){
        cy.get('#submitAddress').click()
    }
}

export default new CompraPage;